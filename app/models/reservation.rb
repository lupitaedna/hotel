class Reservation < ApplicationRecord
	has_many :restaurants
	belongs_to :user
	
	validates :fecha_registro, presence: true
	validates :cantidad, numericality: {only_integer: true, message: "Solo numeros enteros"}
	validates :fecha_final, presence: true
end
