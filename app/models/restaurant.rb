class Restaurant < ApplicationRecord
	belongs_to :reservation
	validates :area, presence: true
	validates :acceso, presence: true
	validates :personas, numericality: {only_integer: true, message: "Solo numeros enteros"}
end
