Rails.application.routes.draw do
  
  devise_for :admins, controllers: { sessions: 'admins/sessions' } 
  devise_for :emails
  devise_for :users, controllers: { sessions: 'users/sessions' }

  resources :reservations do
  resources :restaurants
end
  root to: "reservations#index"

  devise_scope :admin do
	get 'admins/users/list', to: 'admins/sessions#list', as: 'users_list'
end

  get'/restaurantes', to: 'restaurants#index', as: 'restaurants'
end
