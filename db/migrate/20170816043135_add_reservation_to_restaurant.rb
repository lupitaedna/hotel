class AddReservationToRestaurant < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :reservation_id, :integer
  end
end
