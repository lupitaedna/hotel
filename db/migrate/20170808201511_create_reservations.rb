class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      t.date :fecha_registro
      t.date :fecha_final
      t.string :mounstro
      t.integer :cantidad

      t.timestamps
    end
  end
end
