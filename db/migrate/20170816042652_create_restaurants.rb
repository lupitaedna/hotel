class CreateRestaurants < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurants do |t|
      t.string :area
      t.boolean :acceso
      t.integer :personas

      t.timestamps
    end
  end
end
